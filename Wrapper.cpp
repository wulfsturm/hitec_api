__declspec(dllimport) void RegisterURL(char* text);
__declspec(dllexport) char* CallWebService(int sessionId, int csLen, char* inCS, bool expect, int* bsLen, char** outBS, bool* contin, int* webResult);

extern "C" void WrapRegisterWebServiceURL(char* variable)
{
	RegisterURL(variable);
}
extern "C" int WrapMainRPC(unsigned long sessionId, unsigned long inDatalength, void* inData, unsigned short expect, long* outDatalength, char** outData, unsigned short* contin)
{
	bool bExpect = expect;
	bool bContin;
	int webResult;
	char* retString = CallWebService((int)sessionId, inDatalength, (char*) inData, bExpect, (int*) outDatalength, (char**)outData, &bContin, &webResult);
	if (bContin) *contin = 1;
	else *contin = 0;
	return webResult;
}