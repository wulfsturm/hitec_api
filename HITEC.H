
#ifndef __TESTDLL
#define __TESTDLL
#include <windows.h>

#ifdef __cplusplus
extern "C" {            /* Assume C declarations for C++ */
#endif  

extern void WINAPI GetNextValues(short k,short *action,char *gob,char *menu,char *data,char *code);
extern int WINAPI GetNextSizes(short k,int *gl,short *ml,short *dl,short *el);
extern long WINAPI GetWindowWidth(HWND wnd);
extern void WINAPI RegisterVariable( char *variable, char *data,
												 short len );
extern void WINAPI UnregisterVariable( char *variable );
extern short WINAPI GetVariableLength( char *variable );
extern void WINAPI GetVariable( char *variable, char *data );
extern void WINAPI InstantiatePump( HWND );
extern void WINAPI MyLogging( long level );

extern char * WINAPI GetEnvironmentVar( char *varname );
void WINAPI RegisterVariable(char* variable, char* data, short len);
void WINAPI RegisterWebServiceURL(char* variable);
extern int WINAPI SystemCall( char *command );
extern int WINAPI DirExists( char *strDir );
extern char * WINAPI GetCWD();
extern int WINAPI CWD( char *strDir );
extern int WINAPI GetDLLDefines();

extern void WINAPI SetEnvFile( char *path );
#ifdef _THREADS_
struct RPCStruct
{
	unsigned long sessid;
	unsigned long indl;
	void *inData;
	unsigned short contin;   
};
extern void MakeRPC(void *);
#else
extern short WINAPI MakeRPC(unsigned long,unsigned long,void *,unsigned short);
#endif
extern short WINAPI ThreadRPC( unsigned long, unsigned long, void *, unsigned short);
extern void SetKeyValue( short);
extern void WINAPI ResetKeyValue();
extern char * WINAPI GetKeyValue();
extern int WINAPI TopMessageBox(HWND, char *, char *, char *, char *, int);
extern char *LCase( char *);

struct Variable
{
	char *name;
	char *data;
	int len;   
};


struct Variable *FindVar( char *variable );


typedef enum {
	Returned_Session_ID=1,	
    NewApplication, 
    NewSheet,  
	ModifySheet,
	CloseSheet,
    NewDataWindow,
	ModifyDataWindow,
    NewButton,  
	ModifyButton,
    NewEventDef,
	ModifyEventDef,
    BSMessageBox, 
    CodeString, 
	Ack,			
	Returned_Ascii_Code,	
	NewPicture,
	ModifyPicture,
	NewPictureButton,
	ModifyPictureButton,
	ScratchPadContents,
	ListBoxPopulate,        
	FileContents,
	SetCurrent,
	ReturnMessage,
	NewTreeView,
	ModifyTreeView,
	NewOleControl,
	ModifyOleControl,
	NewModalSheet,
	DDERequest,
	NewModalSheetRefresh,
	ResetTreeView,
	ReopenSheet,
	CloseReopenSheet,
	ReadBSFromFile = 50,
	ReadBSFromMem = 51,
	MemContents = 52
} enum_BS;

#define CS_GET_CACHE_ENTRY 18

typedef struct BSS { 
    enum_BS m_strActionName;
	struct BSS *next;
    char *  m_strGOB;
    int gl;
	char *  m_strMenu; 
	unsigned short ml;
	char *  m_strData;
	unsigned short dl;
    char *  m_strEventCode;
	unsigned short el;
	short key;
	int retval;
} BuildingStruct;

#ifdef STRICT
#define GENWNDPROCTYPE FARPROC
#else
#define GENWNDPROCTYPE WNDPROC
#endif


typedef struct {
	HWND hwnd;
#ifdef STRICT
	FARPROC proc;	
#else
	WNDPROC proc;
#endif
} Proc;



#ifdef __cplusplus
}
#endif  

#endif
