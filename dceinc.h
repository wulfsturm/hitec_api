/* COPYRIGHT_TEXT */

/* __OECVERSION__ */

/*********************************************************************
** File:      DCEINC.H
** Created:   2/10/92
** Modified:
** PURPOSE of this file: Header file for Client and Server Applications
** Comments:  __ARPA__    = ARPA for windows
**	      THINK_C	  = MacTCP for Macintosh
**	      __NEWT__ 	  = NetManage foor windows
**	      __mpexl     = HP3000
**	      odeunix     = All Unix, including _mach_ = NextStep
**	      __ARPADOS__ = ARPA for dos
**	      __FTPDOS__  = FTP for DOS
**	      __FTPWIN__  = FTP for Windows
**	      __OS2__	  = For OS/2 protected mode DLLs
**	      __NOVELL__  = For Novell Lan WorkPlace for Dos & Windows
**            __CLIP__    = Clipper additions
**            __DIST__    = Distinct TCP/IP for Windows
**	      __DLL__     = Predefined by BC++ if compiling for DLLs
**	      __MSDOS__   = Predefined by BC++ if compiling on a PC
**	      SECURITY    = Define it if security option is needed
**	      CPPC	  = Define it if using C++
**	      MEMDBG	  = Used for debugging memory leaks.
***********************************************************************/
#ifndef ODE_DCEINC_HEADER
#define ODE_DCEINC_HEADER


#ifndef OS1100            
#ifdef _MACINTOSH_
#include <types.h>
#else
#include <sys/types.h>   /* Not valid on most Mainframes */
#endif /* THINK_C || __MWERKS__*/
#endif 

#include <stddef.h>  /* for size_t */

#ifndef WIN32
#include <pthread.h>
#if defined(_solaris_)
#include <thread.h>     /* Pick up signal mask stuff */
#endif
#endif
#include "dceerr.h"

#ifdef EXPTYPE
#undef EXPTYPE
#endif

#ifdef ODE_HUGE
#undef ODE_HUGE
#endif

#ifdef l_int
#undef l_int
#endif

#if defined(__BORLANDC__) && defined(__MSDOS__) 

#ifdef _Windows
#include <windows.h>
#define EXPTYPE FAR PASCAL _export
#if defined (__WIN32__) || defined (_WIN32)
#define ODE_HUGE
#else  
#define ODE_HUGE huge 
#endif
#define l_int unsigned long
#else
#define EXPTYPE
#define ODE_HUGE
#define l_int int
#endif

#endif /* __BORLANDC__   __MSDOS__ */

#if defined (__WIN32__) || defined (_WIN32)
#include <windows.h>
#define EXPTYPE APIENTRY
#define ODE_HUGE
#define l_int unsigned int
#endif /* __WIN32__ */ 

#ifdef __MSVC__        
#ifdef l_int
#undef l_int
#endif
#ifdef EXPTYPE
#undef EXPTYPE
#endif                  
#define l_int unsigned long
#define EXPTYPE  __far __pascal __export
#define ODE_HUGE
#define dce_pop_float dce_pop_float_msvc
#define dce_pop_double dce_pop_double_msvc
#define dce_pop_double_str dce_pop_double_str_msvc
#define dce_pop_float_str dce_pop_float_str_msvc
#define dce_pop_double_Dstr dce_pop_double_Dstr_msvc
#define dce_pop_float_Dstr dce_pop_float_Dstr_msvc 
#endif /* __MSVC__ */

#if defined(THINK_C) || defined(__MWERKS__)
#undef l_int    /* undef above definition */
#define l_int unsigned long
#define EXPTYPE
#define ODE_HUGE
#endif  /* THINK_C || __MWERKS__ */

#ifdef __cplusplus
#define CPPC "C"
#else
#define CPPC
#endif

#define close_sock(Socket) dce_close_socket(Socket)

#ifdef __OS2__
#include <os2def.h>

#ifdef ODE_HUGE
#undef ODE_HUGE
#define ODE_HUGE
#else
#define ODE_HUGE
#endif

#define l_int int
#ifdef __BORLANDC__
#define EXPTYPE EXPENTRY
#elif __RPCPERL__
#define EXPTYPE _cdecl
#elif defined(__IBMC__) || defined(__IBMCPP__)
#define EXPTYPE _System
#endif
#endif

#ifndef EXPTYPE
#define EXPTYPE
#endif

#ifndef ODE_HUGE 
#define ODE_HUGE
#endif

#ifndef l_int
#define l_int int
#endif

#ifndef WIN32
#include <odep.h>
#endif

/*********************************************************************
**  Global Defines and Structs
**********************************************************************/
#define VARLEN 100                      /* for variables like hostname port */

struct table {
	char tag[VARLEN];
	char ODE_HUGE *contents;
	long size;
	struct table *next;
	struct table *previous;
	struct table *beginning;
	struct table *end;
};

#ifdef __OS2__
struct Paramstruct {
	char s1[2];
	};
#endif


/********************************************************************
** Modes for dce_set_debugmode
**
*********************************************************************/
#define DCE_NOIGNORE 0
#define DCE_IGNORE   1
#define DCE_QUERYSTATUS 2

/*********************************************************************
**  Read environmental variables, initialize structures and golbals:
**  		in file dceenv.c
**********************************************************************/

extern CPPC int EXPTYPE dce_setenv(char *,char *,char *);
extern CPPC int EXPTYPE dce_close_env(void);
extern CPPC int EXPTYPE dce_set_account(char *,char *);
 
/*********************************************************************
** Functions to handle server search and list updates:
**		in file dcefunc.c
**********************************************************************/

extern CPPC struct table * EXPTYPE dce_submit(char *, char *,int);
extern CPPC struct table * EXPTYPE dce_submit_ext(char *,char *,char *,int,void *);
extern CPPC int EXPTYPE dce_findserver(char *);
extern CPPC int EXPTYPE dce_init_server(char *, char *);
extern CPPC struct table * EXPTYPE dce_waitfor_call(int ,char *);
extern CPPC int EXPTYPE dce_retsocket(void);
extern CPPC void EXPTYPE dce_spawn (int, int, char *[], char*, char*);
extern CPPC int EXPTYPE dce_isDedicated (char *);
extern CPPC void EXPTYPE dce_dedDisconnect (char *);
extern CPPC char * EXPTYPE dce_servername(char *);
extern CPPC int EXPTYPE dce_ping(char *);
extern CPPC void EXPTYPE dce_check_stub_version_against_lib_version(void);
extern CPPC unsigned int EXPTYPE dce_getpeeraddr(void);
extern CPPC int EXPTYPE dce_getpeerstr(char *);
#if !defined(__DLL__) || defined (__OS2__)
extern CPPC long EXPTYPE dce_get_avg_idle(char *, char*);
#endif


/*********************************************************************
**  Functions for dce table access:  in file dceacce.c
**********************************************************************/

extern CPPC void EXPTYPE dce_push_short(int,char *,short);
extern CPPC void EXPTYPE dce_push_short_str(int,char *,short ODE_HUGE *,l_int);
extern CPPC void EXPTYPE dce_push_int(int,char *,int);
extern CPPC void EXPTYPE dce_push_int_str(int,char *,int ODE_HUGE *,l_int);
extern CPPC void EXPTYPE dce_push_long(int,char *,long);
extern CPPC void EXPTYPE dce_push_long_str(int,char *, long ODE_HUGE *,l_int);
/* #if !defined(__DLL__) || defined (__OS2__)  */
extern CPPC void EXPTYPE dce_push_float(int,char *,float);
extern CPPC void EXPTYPE dce_push_double(int,char *,double);
extern CPPC void EXPTYPE dce_push_float_str(int,char *, float ODE_HUGE *, l_int);
extern CPPC void EXPTYPE dce_push_double_str(int,char *,double ODE_HUGE *, l_int);
/*#endif */
extern CPPC void EXPTYPE dce_push_char(int,char *,char);
extern CPPC void EXPTYPE dce_push_char_str(int,char*,char ODE_HUGE *,long);
extern CPPC void EXPTYPE dce_push_char_arr(int,char*,char ODE_HUGE **,long,long);

extern CPPC short EXPTYPE dce_pop_short(struct table *,char *);
extern CPPC void EXPTYPE dce_pop_short_str(struct table *,char *,short ODE_HUGE *,l_int);
extern CPPC int EXPTYPE dce_pop_int(struct table *,char *);
extern CPPC void EXPTYPE dce_pop_int_str(struct table *,char *,int ODE_HUGE *,l_int);
extern CPPC long EXPTYPE dce_pop_long(struct table *,char *);
extern CPPC void EXPTYPE dce_pop_long_str(struct table *,char *,long ODE_HUGE *, l_int);
#ifdef __MSVC__
extern CPPC double EXPTYPE dce_pop_double_msvc(struct table *, char *);
extern CPPC float EXPTYPE dce_pop_float_msvc(struct table *, char *);
extern CPPC void EXPTYPE dce_pop_double_str_msvc(struct table*,char*,double ODE_HUGE *, l_int);
extern CPPC void EXPTYPE dce_pop_float_str(struct table *,char *,float ODE_HUGE *, l_int);
#else
extern CPPC double EXPTYPE dce_pop_double(struct table *,char *);
extern CPPC float EXPTYPE dce_pop_float(struct table *,char *);
extern CPPC void EXPTYPE dce_pop_double_str(struct table*,char*,double ODE_HUGE *, l_int);
 extern CPPC void EXPTYPE dce_pop_float_str(struct table *,char *,float ODE_HUGE *,l_int);
#endif
extern CPPC char EXPTYPE dce_pop_char(struct table *,char *);
extern CPPC void EXPTYPE dce_pop_char_str(struct table *,char *,char ODE_HUGE *,long);
extern CPPC void EXPTYPE dce_pop_char_arr(struct table*,char *,char ODE_HUGE **,long,long);


/*********************************************************************
**  Functions to manipulate the structures of type table:
**              in file dcearra.c
**********************************************************************/

extern CPPC struct table * EXPTYPE dce_table_find(struct table *,char *);
extern CPPC void EXPTYPE dce_table_destroy(struct table *);
extern CPPC int EXPTYPE dce_table_push(int,char *,char ODE_HUGE *,long);
extern CPPC int EXPTYPE dce_table_pop(struct table *,char *,void *,long);


/*********************************************************************
**  Functions to handle socket communications:  in file dceconn.c
**********************************************************************/

extern CPPC int EXPTYPE dce_send(int,char *);
extern CPPC int EXPTYPE dce_recv_conf(int);
extern CPPC int EXPTYPE dce_close_socket(int);


/*********************************************************************
**  Memory allocation routines: in file dcemem.c
**********************************************************************/

#if (defined(__MSDOS__) || defined(__MSVC__))
extern CPPC void ODE_HUGE * EXPTYPE *dce_malloc(size_t size);
extern CPPC void ODE_HUGE * EXPTYPE dce_calloc(size_t n, size_t size);
extern CPPC void ODE_HUGE * EXPTYPE dce_realloc(void  *oldptr, unsigned long size);
#else
extern CPPC void * EXPTYPE dce_malloc(size_t);
extern CPPC void * EXPTYPE dce_calloc(size_t,size_t);
extern CPPC void *EXPTYPE dce_realloc(void *, size_t);
#endif
extern CPPC void EXPTYPE dce_release(void);
extern CPPC void EXPTYPE dce_free(void *);

/*********************************************************************
**  Dynamic array mainpulation functions: in file dcedyn.c
**********************************************************************/

extern CPPC void * EXPTYPE dce_pop_Dvoid(struct table *,char *);
extern CPPC void EXPTYPE dce_push_char_Nstr(int ,char *,char ODE_HUGE *);
extern CPPC char ODE_HUGE * EXPTYPE dce_pop_char_Nstr(struct table *,char *);
extern CPPC int EXPTYPE dce_push_char_Narr(int ,char *,char ODE_HUGE **);
extern CPPC char ODE_HUGE ** EXPTYPE dce_pop_char_Narr(struct table *,char *);
extern CPPC void EXPTYPE dce_push_short_Dstr(int,char *,short ODE_HUGE *,l_int);
extern CPPC void EXPTYPE dce_push_long_Dstr(int,char *,long ODE_HUGE *,l_int);
extern CPPC void EXPTYPE dce_push_int_Dstr(int,char *,int ODE_HUGE *,l_int);
extern CPPC void EXPTYPE dce_push_float_Dstr(int,char *,float ODE_HUGE *,l_int);
extern CPPC void EXPTYPE dce_push_double_Dstr(int,char *,double ODE_HUGE *,l_int);
#ifdef __MSVC__
extern CPPC float ODE_HUGE * EXPTYPE dce_pop_float_Dstr_msvc(struct table *,char *,l_int);
extern CPPC double ODE_HUGE * EXPTYPE dce_pop_double_Dstr_msvc(struct table *,char * ,l_int);
#else
extern CPPC float ODE_HUGE * EXPTYPE dce_pop_float_Dstr(struct table *,char *,l_int);
extern CPPC double ODE_HUGE * EXPTYPE dce_pop_double_Dstr(struct table *,char *,l_int);
#endif
extern CPPC int ODE_HUGE * EXPTYPE dce_pop_int_Dstr(struct table *,char *,l_int);
extern CPPC short ODE_HUGE * EXPTYPE dce_pop_short_Dstr(struct table *,char *,l_int);
extern CPPC long ODE_HUGE * EXPTYPE dce_pop_long_Dstr(struct table *,char *,l_int);

/* macros used for other conversions */
#define dce_pop_void_Dstr(rec,tag,len) dce_pop_Dvoid(rec,tag)
#define dce_pop_void_str(rec,tag,blob,size) dce_table_pop(rec,tag,blob,size)
#define dce_push_void_str(Soc,tag,blob,size) dce_table_push(Soc,tag,blob,size)
#define dce_push_Dvoid(Soc,tag,blob,size) dce_table_push(Soc,tag,blob,size)
#define dce_push_void_Dstr(Soc,tag,blob,size) dce_table_push(Soc,tag,blob,size)
#define dce_push_char_Dstr(S,tag,data,n) dce_push_char_str(S,tag,data,n)
#define dce_push_char_Darr(S,tag,data,n,m) dce_push_char_arr(S,tag,data,n,m)
#define dce_pop_char_Dstr(rec,tag,len) dce_pop_char_Nstr(rec,tag)
#define dce_pop_char_Darr(rec,tag,n,m) dce_pop_char_Narr(rec,tag)


/*********************************************************************
**  General Utilities existing:  in file dceutils.c
**********************************************************************/

extern CPPC int EXPTYPE dce_error(char *);
extern CPPC void EXPTYPE dce_clearerror(void);
extern CPPC void EXPTYPE dce_version (char *);
extern CPPC char * EXPTYPE dce_errstr(void);
extern CPPC int EXPTYPE dce_errnum(void);
extern CPPC void EXPTYPE dce_set_exit(void);
extern CPPC int EXPTYPE dce_should_exit(void);
extern CPPC void EXPTYPE dce_checkver(int, int);
extern CPPC int EXPTYPE dce_err_is_fatal(void);
extern CPPC int EXPTYPE dce_server_is_ded(void);
#ifdef __OS2__
#ifdef __RPCPERL__
extern CPPC void EXPTYPE dce_unknown_func(char *func, long table, int Socket);
#else
extern CPPC void EXPTYPE dce_unknown_func(char *func, struct table * dce_table, int Socket);
#endif
#elif defined(__WIN32__) || defined (_WIN32)
extern CPPC void EXPTYPE dce_unknown_func(char *func, struct table * dce_table, int Socket);
#else
extern CPPC void EXPTYPE dce_unknown_func(char *func, struct table * dce_table, int Socket);
#endif
/* dll clear busy flag ***/
extern CPPC void EXPTYPE dce_busy_clear(void);


/**********************************************************************
**  Argument Parsing routines
***********************************************************************/

extern CPPC int EXPTYPE parse_args(int *,char **,char **);
extern CPPC int EXPTYPE dce_validarg(char *);


/*********************************************************************
** Logging. in dce_log.c
**********************************************************************/
#ifndef __MSVC__
extern CPPC void EXPTYPE dce_dbgwrite(int level, char *fmtstr, ...);
#endif
extern CPPC void EXPTYPE dce_log_str(char *msg);

/*********************************************************************
** Broker query functions in brokercl.c
**********************************************************************/

extern CPPC int EXPTYPE dce_querybroker(char **);
extern CPPC int EXPTYPE dce_brokrpc_get_instances(char*, char*, int, char**);

/*********************************************************************
** Functions exported for DCEPERL
**********************************************************************/

extern CPPC struct table * EXPTYPE dce_table_build(void);
extern CPPC struct table * EXPTYPE dce_element_add(struct table *,char *);
extern CPPC int EXPTYPE dce_send_array(int ,char *,struct table *);
extern CPPC long EXPTYPE dce_element_get(struct table *,char *,char ODE_HUGE *,long );

/*********************************************************************
** Function for window input control
*********************************************************************/
extern CPPC int EXPTYPE dce_ignore_input(int mode);

/*********************************************************************
** Logging Constants
**********************************************************************/
#define DCE_LOG_NONE          0
#define DCE_LOG_ERROR         6
#define DCE_LOG_WARNING       19
#define DCE_LOG_DEBUG         29


/*********************************************************************
** Functions exported for DCEMT
**********************************************************************/
#ifndef WIN32
int dce_dispatch_rpc (char * dce_func, int rsocket,
        struct table * dce_table, void* rpc_handler);
void dce_server_queue_init(void);
void dce_svr_setup(void);
#endif

#endif /* ODE_DCEINC_HEADER, for multiple-inclusion protection */
/* Don't add anything after this */



