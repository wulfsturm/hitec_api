#include "hitec.h"
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include "Wrapper.h"



struct Variable *Variables=NULL;
int NumVars = 0;
    
struct Variable *FindVar( char *variable )
{
	int i;
	
	for(i=0;i<NumVars;i++) 
		if( !strcmp(Variables[i].name,variable ) )
			return &(Variables[i]);
	return NULL;
}

void WINAPI RegisterVariable( char *variable, char *data,short len )
{
	if( !variable || !data || !*variable || !len || FindVar(variable) ) 
		return;
	
	Variables = (struct Variable *)realloc(Variables,(NumVars+1)*sizeof(struct Variable));
	Variables[NumVars].name = strdup(variable);
	if ( (short)strlen(data) > (len - 1)) len = (short)strlen(data)+1;
	Variables[NumVars].data = (char *)malloc(len*sizeof(char));
	Variables[NumVars].len = len;
	memcpy(Variables[NumVars].data,data,len);  
	NumVars++;
}


void WINAPI RegisterWebServiceURL(char* variable)
{
	WrapRegisterWebServiceURL(variable);
}
	
void WINAPI UnregisterVariable( char *variable )
{   
	int i;                               
	struct Variable *visit;
	
	if( !variable || !*variable || !(visit=FindVar(variable)) )
		return;
	
	free(visit->name);
	free(visit->data);
	      
	for(i = visit-Variables;i<(NumVars-1);i++)
		memcpy(Variables+i,Variables+i+1,sizeof(struct Variable));
		
	Variables = (struct Variable *)realloc(Variables,(--NumVars)*sizeof(struct Variable));
}	
	
	
	
short WINAPI GetVariableLength( char *variable )
{                      
	struct Variable *visit;
	if ( !variable || !(visit=FindVar(variable)) )
		return 0;
		
	return visit->len;
}

void WINAPI GetVariable( char *variable, char *data )
{

	struct Variable *visit;
	if ( !variable || !(visit=FindVar(variable)) )
		return;
		
	memcpy(data,visit->data,visit->len);
}

